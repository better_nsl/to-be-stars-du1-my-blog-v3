export interface RightListItemInter{
    id:number | string,
    name:string,
    isSelected:boolean,
    children?:RightListItemInter[],
    aside?:string,
    anchor?:string
}