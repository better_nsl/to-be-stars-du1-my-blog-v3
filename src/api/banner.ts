import { AxiosPromise } from "axios";
import { InterfaceBanner } from "./apiTypes";
import { aRequest } from "./request"; 

export async function getBannersApi():Promise<InterfaceBanner[]>{
    return await aRequest.get('/api/banner');
}