import { TypeApiProjectI } from "./apiTypes";
import { aRequest } from "./request";

export async function apiGetProject():Promise<TypeApiProjectI[]>{
    return await aRequest.get('/api/project')
}