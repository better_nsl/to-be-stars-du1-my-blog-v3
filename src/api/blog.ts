import { aRequest } from "./request";
import { TypeApiBlogCategoryI, TypeApiBlogDetailI, TypeApiBlogPageI,TypeApiCommentI,TypeApiCommentPageI, TypeApiCommentPostI } from "./apiTypes";


export async function getBlogsApi(
        page=1,limit=10,catagoryId=-1
    ) :Promise<TypeApiBlogPageI>{
    return await aRequest.get('/api/blog',{
        params:{
            page,
            limit,
            catagoryId
        }
    });
}

export async function getBlogCategories():Promise<TypeApiBlogCategoryI[]> {
    return await aRequest.get('/api/blogtype')
}

export async function getBlog(id:number):Promise<TypeApiBlogDetailI> {
    return await aRequest({
        method:'get',
        url:`/api/blog/${id}`
    })
}

export async function postComment(commentInfo:TypeApiCommentPostI):Promise<TypeApiCommentI> {
    return await aRequest.post(`/api/comment`,commentInfo);
}

export async function getComments(blogId:number,page=1,limit=10):Promise<TypeApiCommentPageI>{
    return await aRequest.get('/api/comment',{
        params:{
            blogId,
            page,
            limit
        }
    })
}


