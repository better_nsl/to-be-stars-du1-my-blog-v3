import { aRequest } from "./request";


export async function apiGetAbout():Promise<string>{
    return await aRequest.get('/api/about')
}