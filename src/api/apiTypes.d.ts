import exp from "constants";

export interface InterfaceBanner {
    id: string;
    midImg: string;
    bigImg: string;
    title: string;
    description: string;
}

export interface BlogRowInterface {
    id: string;
    title: string;
    description: string;
    category: {
        id: number;
        name: string;
    };
    scanNumber: number;
    commentNumber: number;
    thumb: string;
    createDate: string;
}

export interface TypeApiBlogPageI {
    total: number;
    rows: BlogRowInterface[];
}

export interface TypeApiBlogCategoryI {
    id: number;
    name: string;
    articleCount: number;
    order: number;
}

export interface TypeApiCommentI {
    id: string;
    nickName: string;
    content: string;
    createDate?: number;
    avatar?: string;
}

export interface TypeApiCommentPageI {
    total: number;
    rows: TypeApiCommentI[];
}

export interface TypeApiBlogDetailI {
    id: string;
    title: string;
    creatDate: number | string;
    scanNumber: number;
    commentNumber: number;
    category: {
        name: string;
        id: number;
    };
    htmlContent: string;
    toc?: any;
}

export interface TypeFormData {
    nickName: {
        value: string;
        error: string;
    };
    content: {
        value: string;
        error: string;
    };
}

export interface TypeApiCommentPostI extends TypeApiCommentI {
    blogId: number;
}

export interface TypeApiSettingI {
    avatar: string;
    siteTitle: string;
    github: string;
    qq: string;
    qqQrCode: string;
    weixin: string;
    weixinQrCode: string;
    mail: string;
    icp: string;
    githubName: string;
    favicon: string;
}

export interface TypeApiProjectI{
    id:string,
    name:string,
    url:string,
    github:string,
    description:string,
    thumb:string
}
