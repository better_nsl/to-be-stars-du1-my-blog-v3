import { TypeApiSettingI } from "./apiTypes";
import { aRequest } from "./request";

export async function apiGetSettings():Promise<TypeApiSettingI> {
    return await aRequest.get('/api/setting');
}