import { TypeApiCommentI, TypeApiCommentPageI, TypeFormData } from "./apiTypes";
import { aRequest } from "./request";

export async function apiGetMessages(page=1,limit=10):Promise<TypeApiCommentPageI> {
    return await aRequest.get('/api/message',{
        params:{
            page,
            limit
        }
    })
}


export async function apiPostMessage(msgInfo:TypeFormData):Promise<TypeApiCommentI>{
    return await aRequest.post('/api/message',msgInfo);
}