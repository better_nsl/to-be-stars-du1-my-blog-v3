import axios from "axios";
import { PopMessageEnum } from "~@/components/Icon/types";
import { showMessage } from "~@/components/PopMessage/showMessage";


const ins=axios.create();
ins.interceptors.response.use(function(resp){
    const msg=resp.data.msg?resp.data.msg:"错误";

    if(resp.data.code!=0){
        showMessage({
            aContent:msg,
            aMessageType:PopMessageEnum.ERROR,
        });
        return null;
    }
    return resp.data.data;
})

export {ins as aRequest}