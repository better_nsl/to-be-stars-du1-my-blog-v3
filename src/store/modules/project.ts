import { defineStore } from "pinia";
import { ref } from "vue";
import { TypeApiProjectI } from "~@/api/apiTypes";
import { apiGetProject } from "~@/api/project";

export const useProjectStore=defineStore('project',()=>{
    const isLoadingRef=ref(true);
    const storeDataRef=ref<TypeApiProjectI[]>();

    const actionGetProject=async ()=>{
        if(storeDataRef.value){
            return;
        }
        isLoadingRef.value=true;
        storeDataRef.value=await apiGetProject();
        isLoadingRef.value=false;
    }

    return {
        isLoadingRef,
        storeDataRef,
        actionGetProject
    }
})