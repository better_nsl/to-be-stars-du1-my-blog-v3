import { defineStore } from "pinia";
import { ref } from "vue";
import { apiGetAbout } from "~@/api/about";

export const useAboutStore=defineStore('about',()=>{
    const loadingRef=ref(true);
    const dataRef=ref('')

    const storeGetAbout=async ()=>{
        if(dataRef.value){
            return;
        }
        loadingRef.value=true;
        dataRef.value=await apiGetAbout();
        loadingRef.value=false;
    }

    return {
        loadingRef,
        dataRef,
        storeGetAbout
    }
})