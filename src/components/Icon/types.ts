export enum FontTypeEnum{
    Blog='blog',
    Home='home',
    Empty='empty',
     
}

export enum PopMessageEnum{
    INFO='home',
    WARN='blog',
    SUCCESS='empty',
    ERROR='blog',
}