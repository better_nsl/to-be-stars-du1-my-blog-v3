import MessageConstructor from './message.vue'
import { createVNode, render } from 'vue'

export function Message(props:object,appendTo:HTMLElement=document.body){
    const propsInFn={
        ...props,
        onDestory:()=>{
            console.log('in onDestory');
            //卸载组件,触发unMounted
            //render(null,container);
        }
    }
    
    const container = document.createElement('div')
    console.log('MessageConstructor',MessageConstructor);
    
    
    const vnode=createVNode(MessageConstructor,propsInFn,null);

    
    const vm=vnode.component;
    console.log('vm',vm);
    
    
    
    console.log('vnode',vnode);
    console.log('vnode.el',vnode.el);
    
    
    
    render(vnode,container);

    new Promise((r)=>{
        r(1)
    }).then(()=>{
        console.log('in promise');
        
        console.log('vnode.el',vnode.el);
        console.log('vnode.component',vnode.component);
    }   
    )
    
    setTimeout(()=>{
        console.log('in setTimeout');
        console.log('vnode.el',vnode.el);
    },500);
    
    
    // const appendTo = document.body
    appendTo.appendChild(container.firstElementChild as Node);
    
    return vnode.component;
}




