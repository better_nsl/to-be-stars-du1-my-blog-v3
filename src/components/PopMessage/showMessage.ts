import { PopMessageEnum } from '../Icon/types';
import MessageConstructor from './message.vue'
import { renderComponent } from '~@/untils/UtilVue'

export type TypeShowMessage={
    aMessageType:PopMessageEnum,
    aContent:string,
    aCallback?:Function
}

export function showMessage(props:TypeShowMessage){
    const _props={
        ...props,
        onDestory:()=>{
            component.myUnmounted()
        }
    }
    const component=renderComponent(MessageConstructor,_props);
    document.body.appendChild(component.vnode!.el as Node);
    if(props.aCallback){
        props.aCallback();
    }
}

