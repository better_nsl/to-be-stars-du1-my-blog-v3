export interface InterfacePagerI{
    total:number,
    limit:number,
    current:number,
    visibleNumber:number
}