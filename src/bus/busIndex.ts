//引入mitt插件:mitt一个方法,方法执行会返回bus对象
import mitt from 'mitt';
import { onMounted, onUnmounted } from 'vue';

type TypeEvent = {
    mainScroll:undefined | HTMLElement,
    setMainScroll:number
};

const $bus = mitt<TypeEvent>();
export  { $bus };

export function bindMainScrollEvent(callback:(para: undefined | HTMLElement)=>void){
    onMounted(()=>{
        $bus.on('mainScroll',callback);
    });

    onUnmounted(()=>{
        $bus.off('mainScroll',callback);
    })
}

export function bindSetScrollEvent(callback:(para: number)=>void){
    onMounted(()=>{
        $bus.on('setMainScroll',callback);
    });

    onUnmounted(()=>{
        $bus.off('setMainScroll',callback);
    })
}


    