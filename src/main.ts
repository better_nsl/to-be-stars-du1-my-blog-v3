import { createApp } from 'vue'
import App from './App.vue'
import MyApp from '~@/MyApp.vue'
import router from './router/routerIndex.ts'
import "./styles/global.less";
import { pinia } from './store/storeIndex.ts';
import { useSettingStoreWithoutSetup } from './store/modules/settings.ts';

const app=createApp(MyApp);
app.use(router);
app.use(pinia);
app.mount('#app');


const settingStore=useSettingStoreWithoutSetup();
settingStore.fetchSetting();
var myName='nsl';

// console.log('import.meta =',import.meta);
// console.log('import.meta.env =',import.meta.env);
// console.log('process.env',process.env);
// console.log('process.env.NODE_ENV',process.env.NODE_ENV);



