import { RouteRecordRaw } from 'vue-router';
import RightListTest from '~@/views/blog/rightList/RightListTest.vue';
import ViewTestIndex from '~@/test/views/ViewTestIndex.vue';
import BlogListTest from '~@/views/blog/components/BlogListTest.vue';

import BlogCategoryTest from '~@/views/blog/components/BlogCategoryTest.vue';

import BlogDetailTest from '~@/views/blog/components/BlogDetailTest.vue';
import NotFoundIndex from '~@/views/notFound/NotFoundIndex.vue';

const viewTestRoutes:RouteRecordRaw[]=[{
    name:'viewTest',
    path:'/viewTest',
    component:ViewTestIndex,
    meta:{
        title:'View Test 测试'
    },
    redirect:{
        name:'rightListTest'
    },
    children:[
        {
            name:'rightListTest',
            path:'/rightListTest',
            component:RightListTest,
            meta:{
                title:'rightListTest'
            },
        },{
            name:'BlogListTest',
            path:'BlogListTest',
            component:BlogListTest,
            meta:{
                title:'BlogListTest'
            },
        },
        {
            name:'BlogCategoryTest',
            path:'BlogCategoryTest',
            component:BlogCategoryTest,
            meta:{
                title:'BlogCategoryTest'
            },
        },
        {
            name:'BlogDetailTest',
            path:'BlogDetailTest',
            component:BlogDetailTest,
            meta:{
                title:'BlogDetailTest'
            },
        },
        {
            name:'NotFoundIndex',
            path:'NotFoundIndex',
            component:NotFoundIndex,
            meta:{
                title:'NotFoundIndex'
            },
        },
    ]
}]

export {viewTestRoutes};