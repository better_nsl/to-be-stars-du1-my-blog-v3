import Test from '~@/test/index.vue';
import IconTest from '~@/components/Icon/test.vue'
import AvatarTest from '~@/components/Avatar/test.vue'
import LayoutTest from '~@/components/Layout/test.vue'
import EmptyTest from '~@/components/Empty/test.vue'
import ImageLoaderTest from '~@/components/ImageLoader/test.vue'
import PagerTest from '~@/components/Pager/test.vue'
import ContactTest from '~@/components/SiteAside/Contact/test.vue'
import MenuTest from '~@/components/SiteAside/Menu/test.vue';
import SiteAsideTest from '~@/components/SiteAside/test.vue';
import PopMessageTest from '~@/components/PopMessage/test.vue'
import RenderComponentTest from '~@/test/UntilsVueTest/RenderComponentTest.vue';
import PopMessageTest2 from '~@/components/PopMessage/test2.vue'
import { RouteRecordRaw } from 'vue-router';
import DataFormTest from '~@/components/MessageArea/DataFormTest.vue';
import DataListTest from '~@/components/MessageArea/DataListTest.vue';
import MesaageAreaIndexTest from '~@/components/MessageArea/MesaageAreaIndexTest.vue';

const componentTestRoutes:RouteRecordRaw[]=[{
    name:'test',
    path:'/test',
    component:Test,
    meta:{
        title:'测试'
    },children:[
        {
            name:'iconTest',
            path:'icon',
            component:IconTest,
            meta:{
                title:'iconTest'
            },
        },
        {
            name:'avatarTest',
            path:'avatar',
            component:AvatarTest,
            meta:{
                title:'avatarTest'
            },
        },
        {
            name:'layoutTest',
            path:'layout',
            component:LayoutTest,
            meta:{
                title:'layoutTest'
            },
        },
        {
            name:'emptyTest',
            path:'empty',
            component:EmptyTest,
            meta:{
                title:'emptyTest'
            },
        },
        {
            name:'imageLoaderTest',
            path:'imageLoader',
            component:ImageLoaderTest,
            meta:{
                title:'imageLoaderTest'
            },
        },
        {
            name:'pagerTest',
            path:'pager',
            component:PagerTest,
            meta:{
                title:'pagerTest'
            },
        },
        {
            name:'contactTest',
            path:'contact',
            component:ContactTest,
            meta:{
                title:'pagerTest'
            },
        },
        {
            name:'menuTest',
            path:'menu',
            component:MenuTest,
            meta:{
                title:'menuTest'
            },
        },
        {
            name:'siteAsideTest',
            path:'siteAside',
            component:SiteAsideTest,
            meta:{
                title:'siteAsideTest'
            },
        },
        {
            name:'popMessageTest',
            path:'popMessageTest',
            component:PopMessageTest,
            meta:{
                title:'popMessageTest'
            },
        },
        {
            name:'PopMessageTest2',
            path:'PopMessageTest2',
            component:PopMessageTest2,
            meta:{
                title:'PopMessageTest2'
            },
        },
        {
            name:'RenderComponentTest',
            path:'RenderComponentTest',
            component:RenderComponentTest,
            meta:{
                title:'RenderComponentTest'
            },
        },
        {
            name:'DataFormTest',
            path:'DataFormTest',
            component:DataFormTest,
            meta:{
                title:'DataFormTest'
            },
        },
        {
            name:'DataListTest',
            path:'DataListTest',
            component:DataListTest,
            meta:{
                title:'DataListTest'
            },
        },
        {
            name:'MesaageAreaIndexTest',
            path:'MesaageAreaIndexTest',
            component:MesaageAreaIndexTest,
            meta:{
                title:'MesaageAreaIndexTest'
            },
        }
    ]
}]

export {componentTestRoutes}