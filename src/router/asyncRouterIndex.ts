import 'nprogress/nprogress.css'
import {start,done,configure} from 'nprogress';
configure({
    trickleSpeed:20,
    showSpinner:false
});

function delay(duration:number) {
    return new Promise((resolve:Function) => {
      setTimeout(() => {
        resolve();
      }, duration);
    });
}


function getPageComponent(pageCompResolver:any){
    return async()=>{
        start();
        if(import.meta.env.DEV){
          await delay(1500);
        }
        const comp=await pageCompResolver();
        done();
        return comp;
    }
}

/**
 * 仅第一调用会动态加载，之后vue就进行了缓存
 */
export const asyncRoutes= [
  {
    name: "project",
    path: "/project",
    component: getPageComponent(() =>
      import(/* webpackChunkName: "project" */ "~@/views/project/index.vue")
    ),
    meta: {
      title: "项目&效果",
    },
  },
  {
    name: "message",
    path: "/message",
    component: getPageComponent(() =>
      import(/* webpackChunkName: "message" */ "~@/views/message/MessageIndex.vue")
    ),
    meta: {
      title: "留言板",
    },
  }
]