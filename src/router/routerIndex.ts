import {
    createRouter,
    createWebHashHistory,
    createWebHistory,
    RouteRecordRaw,
} from "vue-router";
import Index from "~@/views/index.vue";
import Home from "~@/views/Home/homeIndex.vue";
import About from "~@/views/about/index.vue";
import Blog from "~@/views/blog/index.vue";
import Message from "~@/views/message/MessageIndex.vue";
import Project from "~@/views/project/index.vue";
import NotFoundIndex from "~@/views/notFound/NotFoundIndex.vue";

import BlogIndex from "~@/views/blog/blogIndex.vue";
import DetailIndex from "~@/views/blog/DetailIndex.vue";

import { componentTestRoutes } from "./componentTestRoutes";
import { apiTestRoutes } from "./apiTestRoutes";
import { viewTestRoutes } from "./viewTestRoutes";
import { asyncRoutes } from "./asyncRouterIndex";

import { setRouteTitle } from "~@/untils/titleController";

let routes: RouteRecordRaw[] = [
    {
        name: "index",
        path: "/",
        component: Index,
        meta: {
            title: "首页",
        },
        redirect: "/home",
        children: [
            {
                name: "home",
                path: "/home",
                component: Home,
                meta: {
                    title: "首页",
                },
            },
            {
                name: "blog",
                path: "/blog",
                component: BlogIndex,
                meta: {
                    title: "文章",
                },
            },
            {
                name: "categoryBlog",
                path: "/categoryBlog/:categoryId",
                component: BlogIndex,
                meta: {
                    title: "文章",
                },
            },
            {
                name: "blogDetail",
                path: "/blog/:id",
                component: DetailIndex,
                meta: {
                    title: "文章-xxxx",
                },
            },
            {
                name: "about",
                path: "/about",
                component: About,
                meta: {
                    title: "关于",
                },
            },
            {
                name:'notFound',
                path:'/:pathMatch(.*)*',
                component:NotFoundIndex
            },
            ...asyncRoutes,
            //使用异步方式加载
            // {
            //     name:'message',
            //     path:'/message',
            //     component:Message,
            //     meta:{
            //         title:'留言板'
            //     },
            // },
            // {
            //     name:'project',
            //     path:'/project',
            //     component:Project,
            //     meta:{
            //         title:'项目效果'
            //     },
            // }
            
        ]
    }
];

routes = routes.concat(componentTestRoutes, apiTestRoutes, viewTestRoutes);

console.log(routes);

const router = createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior: () => ({ left: 0, top: 0 }),
});

export default router;

export { componentTestRoutes };

router.afterEach((to, _from) => {
    if (to.meta.title) {
        setRouteTitle(to.meta.title as string);
    }
});
