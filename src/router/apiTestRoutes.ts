import { RouteRecordRaw } from 'vue-router';
import ApiTest from '~@/test/api/ApiTest.vue';

const apiTestRoutes:RouteRecordRaw[]=[{
    name:'testApi',
    path:'/testApi',
    component:ApiTest,
    meta:{
        title:'Api测试'
    }
}]

export {apiTestRoutes};