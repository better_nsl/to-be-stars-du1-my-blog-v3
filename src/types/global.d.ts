declare type GloTypeObjValueString={
    [p:string]:string
}

/**
 * 指令bindings参数类型 
 * mounted(el, binding, vnode, prevVnode) {}
 * 里面的bindings参数类型
 */
declare type GloTypeDirectiveBindings={
    arg: string,
    modifiers:GloTypeObjValueString,
    value: any /* value of `baz` */,
    oldValue: any /* value of `baz` from previous update */
}
/**
 * 从vue3 中找到的定义，但vue3并未将此接口export
 */
declare interface  GloTypeClassComponentCla {
    new (...args: any[]): ComponentPublicInstance<any, any, any, any, any>;
    __vccOpts?: ComponentOptions; //增加?, 避免ts报错
} 