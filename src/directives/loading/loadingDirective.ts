import loadingSvgUrl from '~@/assets/loading.svg';
import styles from './loading.module.less';

function getLoadingImage(el:HTMLElement){
    return el.querySelector('img[data-role=loading]');
}

function createLoadingImg(){
    const img=document.createElement('img');
    img.dataset.role='loading';
    img.src=loadingSvgUrl;
    //console.log('styles:',styles);
    img.className=styles.loading;
    return img;
}

// 导出指令的配置对象
const vLoad=(el :HTMLElement ,binding:any)=>{
    //console.log('in vload directive',binding.value);
    
    const imgDom=getLoadingImage(el);

    // 根据 binding.value 的值，决定创建或删除img元素
    if(binding.value){
        if(!imgDom){
            const img=createLoadingImg();
            el.appendChild(img);
        }
    }else{
        if(imgDom){
            imgDom.remove();
        }
    }
}

export {vLoad}