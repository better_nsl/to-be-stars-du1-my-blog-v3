import { createVNode, render } from 'vue'
import type {ComponentPublicInstance,VNodeProps}from 'vue'

interface MyClassComponent{
    new (...args: any[]): ComponentPublicInstance<any, any, any, any, any>;
//   __vccOpts: ComponentOptions;//原定义有
 }
type Data={
    [x: string]: unknown;
}

export function renderComponent(
    componentConstructor:MyClassComponent,
    props: (Data & VNodeProps) | null) {

    const container = document.createElement('div');
    const vnode=createVNode(componentConstructor,props,null);
    render(vnode,container);

    const myUnmounted=()=>render(null,container);
    return {
        ...vnode.component,
        myUnmounted
    };
}


