let routeTitle='',siteTitle='';

export function setTitle(){
    if(!routeTitle && !siteTitle){
        document.title='加载...';
    }else if(routeTitle && !siteTitle){
        document.title=routeTitle;
    }else if(!routeTitle && siteTitle){
        document.title=siteTitle;
    }else{
        document.title=`${routeTitle}-${siteTitle}`;
    }
}

export function setRouteTitle(title:string){
    routeTitle=title;
    setTitle();
}

export function setSiteTitle(title:string){
    siteTitle=title;
    setTitle();
}
