


/**
 * obj2混合到obj1产生新的对象
 */
export const mixin = function (obj1:object, obj2:object) {
    return Object.assign({}, obj1, obj2);
    // var newObj = {};
    // //复制obj2的属性
    // for (var prop in obj2) {
    //     newObj[prop] = obj2[prop];
    // }
    // //找到obj1中有但是obj2中没有的属性
    // for (var prop in obj1) {
    //     if (!(prop in obj2)) {
    //         newObj[prop] = obj1[prop];
    //     }
    // }
    // return newObj;
}

/**
 * 克隆一个对象
 * @param {boolean} deep 是否深度克隆
 */
export const clone = function (obj: any, deep: boolean) {
    if (Array.isArray(obj)) {
        if (deep) {
            //深度克隆
            var newArr:any = [] ;
            for (var i = 0; i < obj.length; i++) {
                newArr.push(clone(obj[i] , deep));
            }
            return newArr;
        }
        else {
            return obj.slice(); //复制数组
        }
    }
    else if (typeof obj === "object") {
        var newObj :any= {};
        for (var prop in obj) {
            if (deep) {
                //深度克隆
                newObj[prop] = clone(obj[prop], deep);
            }
            else {
                newObj[prop] = obj[prop];
            }
        }
        return newObj;
    }
    else {
        //函数、原始类型
        return obj; //递归的终止条件
    }
}


/**
 * 函数防抖: 重新开始
 */
export const debounce = function (callback:()=>any, time:number) {
    var timer:NodeJS.Timeout ;
    return function () {
        clearTimeout(timer);//清除之前的计时
        var args :any= arguments; //利用闭包保存参数数组
        timer = setTimeout(function () {
            callback.apply(null, args);
        }, time);
    }
}

/**
 * 函数节流： 不能打断
 */
export const throttle = function (callback : Function, time:number, immediately:boolean) {
    if (immediately === undefined) {
        immediately = true;
    }
    if (immediately) {
        let t:any;
        return function () {
            if (immediately) {
                if (!t || Date.now() - t >= time) { //之前没有计时 或 距离上次执行的时间已超过规定的值
                    callback.apply(null, arguments);
                    t = Date.now(); //得到的当前时间戳
                }
            }
        }
    }
    else {
        let timer:any;
        return function () {
            if (timer) {
                return;
            }
            var args = arguments; //利用闭包保存参数数组
            timer = setTimeout(function () {
                callback.apply(null, args);
                timer = -1;
            }, time);
        }
    }
}

/**
 * 科里化函数
 * 在函数式编程中，科里化最重要的作用是把多参函数变为单参函数
 */
export const curry = function (func:Function) {
    //得到从下标1开始的参数
    var args = Array.prototype.slice.call(arguments, 1);
    //@ts-expect-error
    var that = this ;
    return function () {
        var curArgs = Array.from(arguments); //当前调用的参数
        var totalArgs = args.concat(curArgs);
        if (totalArgs.length >= func.length) {
            //参数数量够了
            return func.apply(null, totalArgs);
        }
        else {
            //参数数量仍然不够
            totalArgs.unshift(func);
            return that.curry.apply(that, totalArgs);
        }
    }
}
/**
 * 函数管道
 */
export const pipe = function () {
    var args = Array.from(arguments);
    return function (val:any) {
        return args.reduce(function (result, func) {
            return func(result);
        }, val);
        // for (var i = 0; i < args.length; i++) {
        //     var func = args[i];
        //     val = func(val);
        // }
        // return val;
    }
}   
/**
 * 根据传进来的data类型数据，返回形如'2012-01-01'形式的字符串
 * @param timestamp 
 * @returns 
 */
export function formatDate(timestamp:Date | string | number,showTime:boolean=false){
    // console.log(+timestamp);
    
    const date=new Date(+timestamp);
    const month=(date.getMonth()+1).toString().padStart(2,'0');
    const day=date.getDate().toString().padStart(2,'0');
    let str=`${date.getFullYear()}-${month}-${day}`
    if (showTime) {
        const hour = date
          .getHours()
          .toString()
          .padStart(2, "0");
        const minute = date
          .getMinutes()
          .toString()
          .padStart(2, "0");
        const second = date
          .getSeconds()
          .toString()
          .padStart(2, "0");
        str += ` ${hour}:${minute}:${second}`;
      }
    return str;
}