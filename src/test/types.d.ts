import type { Component } from 'vue'
export  interface TestRecord {
    title:string
    name:string,
    description:string,
    component:Component
}