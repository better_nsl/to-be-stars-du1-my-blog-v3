import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path';
import Inspect from 'vite-plugin-inspect';
import { viteMockServe } from 'vite-plugin-mock';
//  process.env.NODE_ENV='production'
//  process.env.NODE_ENV='development'
// https://vitejs.dev/config/
export default defineConfig((parameters)=>{

	console.log('parameters',parameters);
	const mode=parameters.mode;

  console.log('import.meta =',import.meta);
  console.log('process.env.NODE_ENV',process.env.NODE_ENV);
  console.log('process.env',process.env);
  

    return{
    resolve: {
      alias: {
          "~@": path.resolve(__dirname, './src'),
      },
  },
  css:{
    preprocessorOptions:{
      less:{
        additionalData:`@import "~@/styles/var.less";
                        @import "~@/styles/mixin.less";`
      }
    }
  },
    plugins: [
		vue(),
		Inspect(),
		viteMockServe({
			mockPath: './mock',
			enable: mode === 'development',
		})
	],
  }
  }
 )
