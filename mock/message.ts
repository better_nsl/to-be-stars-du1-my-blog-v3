import qs from "querystring";
export default[
    {
        url: "/api/message",
        method: "post",
        response: () => {
            return {
                code: 0,
                msg: "",
                data: {
                    id: "@guid",
                    nickname: "@cname",
                    content: "@cparagraph(1, 10)",
                    createDate: Date.now(),
                    "avatar|1": [
                      "https://qiheizhiya.oss-cn-shenzhen.aliyuncs.com/image/avatar6.jpg",
                      "https://qiheizhiya.oss-cn-shenzhen.aliyuncs.com/image/avatar4.jpg",
                      "https://qiheizhiya.oss-cn-shenzhen.aliyuncs.com/image/avatar8.jpg",
                      "https://qiheizhiya.oss-cn-shenzhen.aliyuncs.com/image/avatar2.jpg",
                    ],
                  },
            };
        },
    },
    {
        url: /^\/api\/message\/?(\?.+)?$/,
        method: "get",
        response: (options:any) => {
            const query = qs.parse(options.url);
            return {
                code: 0,
                msg: "",
                data: {
                    total: 52,
                    [`rows|${query.limit || 10}`]: [
                      {
                        id: "@guid",
                        nickname: "@cname",
                        content: "@cparagraph(1, 10)",
                        createDate: Date.now(),
                        "avatar|1": [
                          "https://qiheizhiya.oss-cn-shenzhen.aliyuncs.com/image/avatar6.jpg",
                          "https://qiheizhiya.oss-cn-shenzhen.aliyuncs.com/image/avatar4.jpg",
                          "https://qiheizhiya.oss-cn-shenzhen.aliyuncs.com/image/avatar8.jpg",
                          "https://qiheizhiya.oss-cn-shenzhen.aliyuncs.com/image/avatar2.jpg",
                        ],
                      },
                    ],
                  },
            };
        },
    },
]